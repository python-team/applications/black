black (25.1.0-2) unstable; urgency=medium

  [ Peter Pentchev ]
  * Team upload.
  * Add the upstream click-mix-stderr patch for click 8.2.0.

  [ Michael R. Crusoe ]
  * Adjust the click 8.2.0 patch to enable the tests to pass.
    Closes: #1098535
  * Standards-Version: 4.7.2 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 27 Feb 2025 15:54:19 +0100

black (25.1.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 05 Feb 2025 09:23:24 +0100

black (25.1.0-0exp) experimental; urgency=medium

  * New upstream version, upload to experimental.
  * Patches, dropped fix-type-error.patch as it was applied upstream.
    Refreshed verbose-mypyc.

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 29 Jan 2025 14:35:48 +0100

black (24.10.0-3) unstable; urgency=medium

  * Team upload.
  * Remove dependency on python3-pkg-resources (Closes: #1083315)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 13 Jan 2025 20:40:14 +0100

black (24.10.0-2) unstable; urgency=medium

  * Team upload.
  * Fix type error (closes: #1091135).

 -- Colin Watson <cjwatson@debian.org>  Mon, 30 Dec 2024 00:48:10 +0000

black (24.10.0-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * d/patches/verbose-mypyc: set the debug level to the default of 2

  [ Sylvestre Ledru ]
  * New upstream release
  * Update Standards-Version to 4.7.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 13 Oct 2024 23:26:13 +0200

black (24.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Refreshed patches.
  * d/control: build the docs using upstream's preferred theme: furo.
  * python3-hatch-fancy-pypi-readme & python3-myst-parser are available,
    remove patches that worked around their previous absencse.
  * Increased (mypyc) build verbosity and produce debug information
  * Link the docs against the python3-doc package.
  * Added patch to preserve privacy by removing external image
    references.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 04 Aug 2024 16:12:54 +0200

black (24.4.2-2) unstable; urgency=medium

  * Team upload.
  * Compile Black using mypyc, as upstream does for PyPI. Closes: #1066036
    Tests are now run at build time as well.
  * Remove trailing whitespace in debian/copyright (routine-update)
  * d/control: remove unneeded X-Python3-Version >= 3.6
  * Remaining patches are Debian-specific and do not need forwarding
    upstream.
  * Use pybuild for the autopkgtest

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 09 May 2024 19:18:06 +0200

black (24.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 03 May 2024 10:45:25 +0200

black (24.4.0-2) unstable; urgency=medium

  * Team upload
  * Note that package Breaks python3-pylsp-black (<< 2.0.0-4)

 -- Julian Gilbey <jdg@debian.org>  Fri, 19 Apr 2024 18:00:06 +0100

black (24.4.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release, fixes CVE-2024-21503 (closes: #1067177)
  * Drop defunct python3-typed-ast from autopkgtest dependencies
    (closes: #1067466)
  * Update Standards-Version (no changes needed)

 -- Julian Gilbey <jdg@debian.org>  Fri, 19 Apr 2024 06:26:09 +0100

black (24.2.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 15 Feb 2024 10:49:02 +0100

black (24.1.1-1) unstable; urgency=medium

  * new upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 11 Feb 2024 18:49:45 +0100

black (23.11.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 19 Nov 2023 13:54:15 +0100

black (23.10.1-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 25 Oct 2023 23:39:44 +0200

black (23.10.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 20 Oct 2023 08:30:41 +0200

black (23.9.1-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 28 Sep 2023 16:49:08 +0200

black (23.7.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 21 Jul 2023 10:27:25 +0200

black (23.3.0-1) unstable; urgency=medium

  * upload to unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 13 Jun 2023 13:53:20 +0200

black (23.3.0-1~exp1) experimental; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 10 Jun 2023 13:35:44 +0200

black (23.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 10 Feb 2023 21:55:25 +0100

black (22.12.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 18 Dec 2022 00:43:45 +0100

black (22.10.0-2) unstable; urgency=medium

  * Team upload
  * d/control: Remove unused dependency on python3-typed-ast
    This dependency is only required for Python < 3.8.

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 13 Oct 2022 23:14:35 +0200

black (22.10.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.

  [ Timo Röhling ]
  * New upstream version 22.10.0

 -- Timo Röhling <roehling@debian.org>  Fri, 07 Oct 2022 21:32:44 +0200

black (22.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 22.8.0

 -- Timo Röhling <roehling@debian.org>  Mon, 05 Sep 2022 18:12:46 +0200

black (22.6.0-2) unstable; urgency=medium

  * Team upload.
  * Add bash completion.
    Thanks to Hans-Christoph Steiner (Closes: #968043)
  * Skip blackd tests

 -- Timo Röhling <roehling@debian.org>  Sat, 06 Aug 2022 20:41:25 +0200

black (22.6.0-1) unstable; urgency=medium

  * Team upload.
  * Modernize and simplify packaging
  * New upstream version 22.6.0
  * Bump Standards-Version to 4.6.1

 -- Timo Röhling <roehling@debian.org>  Sat, 30 Jul 2022 23:05:32 +0200

black (22.3.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 05 Apr 2022 09:28:05 +0200

black (22.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 01 Mar 2022 10:20:37 +0100

black (21.12b0-1) unstable; urgency=medium

  * New upstream release.
  * Add myself to Uploaders.
  * Specify Rules-Requires-Root: no.

 -- Chris Lamb <lamby@debian.org>  Fri, 28 Jan 2022 08:05:31 -0800

black (21.10b0-1) unstable; urgency=medium

  [ Neil Williams ]
  * Prepare for upload

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-regex.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.

  [ Neil Williams ]
  * Update homepage links for new location to avoid the redirect.
  * Bump debhelper from old 12 to 13.
  * Update B-D for upstream changes
  * Fix autopkgtest support for new upstream
  * Allow autopkgtest on all architectures (Closes: #995344)
  * New upstream release (Closes: #966141)

 -- Neil Williams <codehelp@debian.org>  Mon, 15 Nov 2021 20:15:11 +0000

black (21.4b2-3) unstable; urgency=medium

  * Remove Sphinxdoc built-using as only symlinks are required.
  * Add black_primer manpage rule & install it.
  * Update manpage for black.1 (Closes: #983440)
  * Update version handling. (Closes: #955008)

 -- Neil Williams <codehelp@debian.org>  Sun, 10 Oct 2021 17:18:11 +0100

black (21.4b2-2) unstable; urgency=medium

  * Team upload.
  * debian/tests/control: drop needs-root restriction
  * debian/tests/control: add new test dependencies (Closes: #994567)

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 28 Sep 2021 18:02:01 -0300

black (21.4b2-1) unstable; urgency=medium

  * Upload in unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 21 Aug 2021 20:40:14 +0200

black (21.4b2-1~exp1) experimental; urgency=medium

  * new upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 01 May 2021 11:28:52 +0200

black (20.8b1-4) unstable; urgency=medium

  * Modify autopkgtests to not use setup.py (Closes: #981764)
    - several tests assume that they run in the source tree, I skipped
      test_process_queue because it assumed there was a json file installed
      near the library.

 -- Diane Trout <diane@ghic.org>  Tue, 09 Feb 2021 11:09:40 -0800

black (20.8b1-3) unstable; urgency=medium

  [ Chris Lamb ]
  * Team upload.
  * Ensure that a "black" binary exists before running the testsuite to avoid test_async_main test failure.
  * releasing package black version 20.8b1-2

  [ Diane Trout ]
  * unittests assume the command is installed (Closes: #972519)
  * Release to unstable

 -- Diane Trout <diane@ghic.org>  Wed, 04 Nov 2020 21:23:13 -0800

black (20.8b1-2) unstable; urgency=medium

  * Team upload.
  * Correct version handling to avoid "ModuleNotFoundError: No module named
    '_black_version'" error when loading. (Closes: #970901)
  * Ensure that a "black" binary exists before running the testsuite to avoid
    test_async_main test failure.
  * Don't specific maintainer-specific build directories in debian/gbp.conf.
  * Commit tarballs to pristine-tar branch.

 -- Chris Lamb <lamby@debian.org>  Mon, 12 Oct 2020 11:48:16 +0100

black (20.8b1-1) unstable; urgency=medium

  [ Denis Laxalde ]
  * d/control: Define Built-Using for -doc package

  [ Sylvestre Ledru ]
  * new upstream release
    Needs sphinx3 (> 3.2) and python3-recommonmark
    and python3-mypy-extensions, python3-typing-extensions
  * black 20.8 needs a recent version of regex, explicit
    the dep: python3-regex (>= 0.1.202018)

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 24 Sep 2020 21:28:48 +0200

black (19.10b0-3) unstable; urgency=medium

  * For now, disable autopkgtest on !amd64
    Fails because of dependencies

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 11 Apr 2020 13:32:13 +0200

black (19.10b0-2) unstable; urgency=medium

  * Try to force the PYTHONPATH to make autopkgtest
    (Closes: #945609)

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 10 Mar 2020 22:37:52 +0100

black (19.10b0-1) unstable; urgency=medium

  [ Emmanuel Arias ]
  * New Upstream release (Closes: #944292)
  * d/control: Bump Standards-Version to 4.4.1
  * d/control: Bump debhelper-compat to 12

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Remove trailing whitespaces.
  * d/control: Fix wrong Vcs-*.

  [ Sylvestre Ledru ]
  * d/control: add missing deps for the testsuite
  * Disable the scm setuptools check as the tarball doesn't
    have the info

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 17 Nov 2019 16:42:25 +0100

black (19.3b0-1) unstable; urgency=medium

  * Upload into unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 07 Jul 2019 09:50:01 +0200

black (19.3b0-1~exp1) experimental; urgency=medium

  * New upstream release (Closes: #926155)
  * Package moved in the Debian Python team (Closes: #926156)

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 09 Apr 2019 22:40:57 +0200

black (18.9b0-1-6) unstable; urgency=medium

  * Support for Python3.7 and test dependencies. (Closes: #914552)

 -- Neil Williams <codehelp@debian.org>  Tue, 27 Nov 2018 09:47:06 +0000

black (18.9b0-1-5) unstable; urgency=medium

  * Bump minimum Python3 version to 3.6 (Closes: #912760)

 -- Neil Williams <codehelp@debian.org>  Wed, 07 Nov 2018 17:34:20 +0000

black (18.9b0-1-4) unstable; urgency=medium

  * Fix "broken symlink to undercore.js in doc package.
    (Closes: #911066)

 -- Neil Williams <codehelp@debian.org>  Mon, 15 Oct 2018 13:22:59 +0100

black (18.9b0-1-3) unstable; urgency=medium

  * Build manpage each time and update VCS links

 -- Neil Williams <codehelp@debian.org>  Mon, 08 Oct 2018 14:34:17 +0100

black (18.9b0-1-2) unstable; urgency=medium

  * Change build-dep to just python3, drop build-depend
    on python3-all-dev (Closes: #910094)

 -- Neil Williams <codehelp@debian.org>  Thu, 04 Oct 2018 12:47:58 +0100

black (18.9b0-1-1) unstable; urgency=medium

  * New upstream release
  * Drop objects.inv from docs to aid reproducibility

 -- Neil Williams <codehelp@debian.org>  Thu, 27 Sep 2018 13:37:28 +0100

black (18.6b4-1) unstable; urgency=medium

  * Initial release. (Closes: #909562: ITP: black -- uncompromising
    Python code formatter)

 -- Neil Williams <codehelp@debian.org>  Fri, 21 Sep 2018 18:43:59 +0100
